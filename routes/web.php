<?php

$router->group(['prefix' => 'v1'], function () use ($router)
{

    $router->get('/', function () use ($router)
    {
        return $router->app->version();
    });

    $router->post('/auth/login', 'AuthController@authenticate');
    $router->post('/auth/register', 'AuthController@register');

    $router->get('/event/picture/{name}', 'EventController@getPicture');
    $router->get('/volunteer/picture/{name}', 'VolunteerController@getPicture');
    $router->get('/donation/picture/{name}', 'DonationController@getPicture');
    $router->get('/community/picture/{name}', 'CommunityController@getPicture');
    $router->get('/personal/picture/{name}', 'PersonalController@getPicture');
    $router->get('/user-volunteer/file/{name}', 'UserVolunteerController@getFile');
    $router->get('/user-volunteer/qr/{name}', 'UserVolunteerController@getQrCode');
    $router->get('/user-donation/file/{name}', 'UserDonationController@getFile');

    $router->group(['middleware' => 'jwt'], function () use ($router)
    {

        $router->get('/get-user/{id}', 'AuthController@getUser');
        $router->post('/change-password/{id}', 'AuthController@changePassword');

        $router->get('/event-featured', 'EventController@highlightEvent');
        $router->get('/volunteer-featured', 'VolunteerController@highlightVolunteer');
        $router->get('/donation-featured', 'DonationController@highlightDonation');

        $router->get('/personal', 'PersonalController@detailPersonal');
        $router->get('/personal/{id}', 'PersonalController@detailPersonalById');
        $router->post('/personal/profile', 'PersonalController@updateProfilePersonal');

        $router->get('/community', 'CommunityController@detailCommunity');
        $router->get('/community/{id}', 'CommunityController@detailCommunityById');
        $router->post('/community/profile', 'CommunityController@updateProfileCommunity');

        $router->get('/event', 'EventController@listAllEvent');
        $router->get('/event/{id}', 'EventController@listEventByUser');
        $router->get('/event/detail/{id}', 'EventController@detailEvent');
        $router->post('/event', 'EventController@createEvent');
        $router->post('/event/{id}', 'EventController@updateEvent');
        $router->delete('/event/{id}', 'EventController@deleteEvent');

        $router->get('/volunteer', 'VolunteerController@listVolunteer');
        $router->get('/volunteer/{id}', 'VolunteerController@listVolunteerByUser');
        $router->get('/volunteer/detail/{id}', 'VolunteerController@detailVolunteer');
        $router->post('/volunteer', 'VolunteerController@createVolunteer');
        $router->post('/volunteer/{id}', 'VolunteerController@updateVolunteer');
        $router->delete('/volunteer/{id}', 'VolunteerController@deleteVolunteer');

        $router->get('/donation', 'DonationController@listAllDonation');
        $router->get('/donation/{id}', 'DonationController@listDonationByUser');
        $router->get('/donation/detail/{id}', 'DonationController@detailDonation');
        $router->post('/donation', 'DonationController@createDonation');
        $router->post('/donation/{id}', 'DonationController@updateDonation');
        $router->delete('/donation/{id}', 'DonationController@deleteDonation');

        $router->get('/user-event', 'UserEventController@listOfUserEvent');
        $router->get('/user-event/{id}', 'UserEventController@registratedUserEvent');
        $router->get('/user-event/user/{id}', 'UserEventController@detailRegistratedUserEvent');
        $router->post('/user-event/{id}', 'UserEventController@createUserEvent');

        $router->post('/user-volunteer/decline/{id}', 'UserVolunteerController@declineUser');
        $router->post('/user-volunteer/accept/{id}', 'UserVolunteerController@acceptUser');
        $router->get('/user-volunteer/user/{id}', 'UserVolunteerController@detailRegistratedUserVolunteer');
        $router->get('/user-volunteer', 'UserVolunteerController@listOfUserVolunteer');
        $router->get('/user-volunteer/{id}', 'UserVolunteerController@registratedUserVolunteer');
        $router->post('/user-volunteer/{id}', 'UserVolunteerController@createUserVolunteer');

        $router->get('/user-donation/user/{id}', 'UserDonationController@detailRegistratedUserDonation');
        $router->get('/user-donation', 'UserDonationController@listOfUserDonation');
        $router->post('/user-donation/proof/{id}', 'UserDonationController@updateProof');
        $router->get('/user-donation/{id}', 'UserDonationController@registratedUserDonation');
        $router->post('/user-donation/{id}', 'UserDonationController@createUserDonation');
        $router->post('/user-donation/decline/{id}', 'UserDonationController@declineDonation');
        $router->post('/user-donation/accept/{id}', 'UserDonationController@confirmDonation');

        $router->get('/article', 'ArticleController@listAllArticle');
        $router->get('/article/detail/{id}', 'ArticleController@detailArticle');
        $router->post('/article', 'ArticleController@createArticle');
        $router->post('/article/{id}', 'ArticleController@updateArticle');
        $router->delete('/article/{id}', 'ArticleController@deleteArticle');

        $router->get('/favorited-event', 'FavoritedEventController@getFavoritedEvent');
        $router->post('/favorited-event/create/{id}', 'FavoritedEventController@createFavoritedEvent');
        $router->delete('/favorited-event/delete/{id}', 'FavoritedEventController@deleteFavoritedEvent');

        $router->get('/favorited-volunteer', 'FavoritedVolunteerController@getFavoritedVolunteer');
        $router->post('/favorited-volunteer/create/{id}', 'FavoritedVolunteerController@createFavoritedVolunteer');
        $router->delete('/favorited-volunteer/delete/{id}', 'FavoritedVolunteerController@deleteFavoritedVolunteer');

        $router->get('/favorited-donation', 'FavoritedDonationController@getFavoritedDonation');
        $router->post('/favorited-donation/create/{id}', 'FavoritedDonationController@createFavoritedDonation');
        $router->delete('/favorited-donation/delete/{id}', 'FavoritedDonationController@deleteFavoritedDonation');
    });
});
