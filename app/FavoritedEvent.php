<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class FavoritedEvent extends Model
{

    use UsesUuid;

    protected $table = 'favorited_event';

    protected $guarded = ['id'];

    protected $fillable = [
        'personal_id',
        'event_id'
    ];
}
