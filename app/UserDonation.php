<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class UserDonation extends Model
{

    use UsesUuid;

    protected $table = 'user_donation';

    protected $guarded = ['id'];

    protected $fillable = [
        'personal_id',
        'donation_id',
        'total_given',
        'status'
    ];

}
