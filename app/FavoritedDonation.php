<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class FavoritedDonation extends Model
{

    use UsesUuid;

    protected $table = 'favorited_donation';

    protected $guarded = ['id'];

    protected $fillable = [
        'personal_id',
        'donation_id'
    ];
}
