<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\Event;
use App\Volunteer;


class Community extends Model
{
    use UsesUuid;

    protected $table = 'community';

    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'address',
        'city',
        'province',
        'post_code',
        'dob',
        'gender',
        'bio',
        'phone',
        'picture'
    ];

    public function event()
    {
        $this->hasMany('App\Event', 'community_id');
    }

    public function volunteer()
    {
        $this->hasMany('App\Volunteer', 'community_id');
    }

}
