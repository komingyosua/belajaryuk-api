<?php

namespace App;

use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Traits\UsesUuid;

use Illuminate\Support\Facades\Hash;

use App\Personal;

class User extends Model
{

    use UsesUuid;

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $fillable =
    [
        'name',
        'password',
        'email',
        'role'
    ];

    protected $hidden = [
        'password'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public static function getCommunityId(Request $request)
    {
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        $community = Community::where('user_id', $credentials->sub)->first();

        return $community->id;
    }

    public static function getPersonalId(Request $request)
    {
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        $personal = Personal::where('user_id', $credentials->sub)->first();

        return $personal->id;
    }

}
