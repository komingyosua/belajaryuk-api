<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Event extends Model
{

    use UsesUuid;

    protected $table = 'event';

    protected $guarded = ['id'];

    protected $fillable = [
        'community_id',
        'title',
        'desc',
        'date',
        'time',
        'place',
        'picture'
    ];

    public function community()
    {
        $this->belongsTo('App\Community', 'id');
    }

}
