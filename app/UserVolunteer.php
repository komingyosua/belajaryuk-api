<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Traits\UsesUuid;

class UserVolunteer extends Model
{

    use UsesUuid;

    protected $table = 'user_volunteer';

    protected $guarded = ['id'];

    protected $fillable =
        [
            'personal_id',
            'volunteer_id',
            'reason',
            'other_document',
            'qr_code'
        ];

}
