<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Article extends Model
{

    use UsesUuid;

    protected $table = 'article';

    protected $guarded = ['id'];

    protected $fillable = [
        'title',
        'description',
        'picture'
    ];

}
