<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Volunteer extends Model
{

    use UsesUuid;

    protected $table = 'volunteer';

    protected $guarded = ['id'];

    protected $fillable =
    [
        'community_id',
        'title',
        'desc' ,
        'date',
        'time',
        'place',
        'quota',
        'picture'
    ];

    public function community()
    {
        $this->belongsTo('App\Community', 'id');
    }

}
