<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Donation extends Model
{

    use UsesUuid;

    protected $table = 'donation';

    protected $guarded = ['id'];

    protected $fillable = [
        'title',
        'description',
        'date',
        'time',
        'place',
        'total_required',
        'picture'
    ];

}
