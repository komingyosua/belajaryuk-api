<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Traits\UsesUuid;

class UserEvent extends Model
{

    use UsesUuid;

    protected $table = 'user_event';

    protected $guarded = ['id'];

    protected $fillable =
    [
        'personal_id',
        'event_id'
    ];

}
