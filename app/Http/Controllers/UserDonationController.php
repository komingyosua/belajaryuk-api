<?php


namespace App\Http\Controllers;


use App\Donation;
use App\User;
use App\UserDonation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UserDonationController
{
    public function __construct()
    {

    }

    public function createUserDonation(Request $request, $id)
    {
        $userDonation = new UserDonation;

        $userDonation->personal_id = User::getPersonalId($request);
        $userDonation->donation_id = $id;
        $userDonation->total_given = $request->input('total_given');
        $userDonation->status = "P";

        $userDonation->save();

        if (!$userDonation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'User Donation has been succesfully created!'
        ], 200);
    }

    public function updateProof(Request $request, $id)
    {
        $userDonation = UserDonation::where('id', $id)->first();

        if($request->has('payment_proof')){
            $file = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('payment_proof')->move($destinationPath, $file);
            $userDonation->payment_proof = $file;
        }

        $userDonation->save();

        if (!$userDonation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Update proof success!'
        ], 200);
    }

    public function listOfUserDonation(Request $request){
        $donation = Donation::join('user_donation', 'user_donation.donation_id', '=', 'donation.id')->get();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function registratedUserDonation(Request $request, $id){
        /* Menunjukkan List User yang mengikuti Event Volunteer berdasarkan ID eventnya */

        $donation = UserDonation::join('personal', 'personal.id', '=', 'user_donation.personal_id')
            ->where('donation_id', '=', $id)
            ->get();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function detailRegistratedUserDonation(Request $request, $id){
        /* Menunjukkan detail user yang terdaftar pada suatu Volunteer Event */

        $donation = UserDonation::join('personal', 'personal.id', '=', 'user_donation.personal_id')
            ->where('personal_id', '=', $id)
            ->get();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function confirmDonation(Request $request, $id){
        $donation = UserDonation::where('personal_id', $id)->first();
        $donation->status = "Y";

        $donation->save();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function declineDonation(Request $request, $id){
        $donation = UserDonation::where('personal_id', $id)->first();
        $donation->status = "N";

        $donation->save();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function getFile($name)
    {
        $file_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($file_path);

        if (file_exists($file_path)) {
            $file = File::get($file_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "File not found";

        return $res;
    }
}