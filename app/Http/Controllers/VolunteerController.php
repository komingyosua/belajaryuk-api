<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Volunteer;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class VolunteerController extends Controller
{

    public function __construct()
    {

    }

    public function listVolunteer()
    {
        $volunteer = Volunteer::get();
        $volunteerCount = count($volunteer);

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer,
            'totalVolunteer' => $volunteerCount
        ], 200);
    }

    public function highlightVolunteer()
    {
        $volunteer = Volunteer::all()->take(3);

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function listVolunteerByUser($id)
    {
        $volunteer = Volunteer::where('user_id', $id)->get();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function detailVolunteer($id)
    {
        $volunteer = Volunteer::where('id', $id)->get();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer,
        ], 200);
    }

    public function createVolunteer(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'desc'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'quota'     => 'required|numeric',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $volunteer = new Volunteer;
        $volunteer->community_id = User::getCommunityId($request);
        $volunteer->title = $request->input('title');
        $volunteer->desc = $request->input('desc');
        $volunteer->date = $request->input('date');
        $volunteer->time = $request->input('time');
        $volunteer->place = $request->input('place');
        $volunteer->quota = $request->input('quota');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $volunteer->picture = $picName;
        }

        $volunteer->save();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Volunteer Event has been succesfully created!'
        ], 200);
    }

    public function updateVolunteer(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'desc'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'quota'     => 'required|numeric',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $volunteer = Volunteer::where('id', $id)->first();

        $volunteer->title = $request->input('title');
        $volunteer->desc = $request->input('desc');
        $volunteer->date = $request->input('date');
        $volunteer->time = $request->input('time');
        $volunteer->place = $request->input('place');
        $volunteer->quota = $request->input('quota');

        if($request->has('picture'))
        {
            $curr_picture_path = storage_path('app/public') . '/' . $volunteer->picture;
            if (file_exists($curr_picture_path)){
                unlink($curr_picture_path);
            }
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $volunteer->picture = $picName;
        }

        $volunteer->save();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Volunteer Event has been succesfully updated!'
        ], 200);
    }

    public function deleteVolunteer($id)
    {
        $volunteer = Volunteer::where('id',$id)->first();

        $volunteer->delete();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Volunteer Event has been succesfully deleted!'
        ], 200);
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }

}