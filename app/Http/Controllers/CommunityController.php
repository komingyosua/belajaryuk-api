<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

use App\Community;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CommunityController extends Controller
{

    public function __construct()
    {

    }

    public function detailCommunity(Request $request)
    {
        $community = Community::where('id', User::getCommunityId($request))->get();

        if (!$community)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $community
        ], 200);
    }

    public function detailCommunityById(Request $request, $id)
    {
        $community = Community::where('id', $id)->first();

        if (!$community)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $community
        ], 200);
    }

    public function updateProfileCommunity(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:255',
            'address' => 'max:255',
            'city' => 'max:64',
            'province' => 'max:64',
            'post_code' => 'numeric',
            'bio' => 'max:255',
            'phone' => 'numeric',
            'picture' => 'image|mimes:jpg,jpeg,png'
        ]);

        $id = User::getCommunityId($request);

        $community = Community::where('id', '=', $id)->first();

        $community->name = $request->input('name');
        $community->address = $request->input('address');
        $community->city = $request->input('city');
        $community->province = $request->input('province');
        $community->post_code = $request->input('post_code');
        $community->bio = $request->input('bio');
        $community->phone = $request->input('phone');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('uploads/personal/images');
            $request->file('picture')->move($destinationPath, $picName);
            $community->picture = $picName;
        }

        $community->save();

        if($community)
        {
            return response()->json([
                'success' => 'Profile has been updated!'
            ], 200);
        }
        else
        {
            return response()->json([
                'error' => 'Something wrong!'
            ], 400);
        }
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('uploads/community/images') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }
}