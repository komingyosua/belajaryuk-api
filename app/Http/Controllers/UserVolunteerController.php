<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Volunteer;
use App\User;
use App\UserVolunteer;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UserVolunteerController extends Controller
{
    public function __construct()
    {

    }

    public function createUserVolunteer(Request $request, $id)
    {
        $userVolunteer = new UserVolunteer;

        $userVolunteer->personal_id = User::getPersonalId($request);
        $userVolunteer->volunteer_id = $id;
        $userVolunteer->reason = $request->input('reason');

        if($request->has('other_document')){
            $file = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('other_document')->move($destinationPath, $file);
            $userVolunteer->other_document = $file;
        }

        if($request->has('qr_code')){
            $file = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('other_document')->move($destinationPath, $file);
            $userVolunteer->qr_code = $file;
        }

        $userVolunteer->status = "P";

        $userVolunteer->save();

        if (!$userVolunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'User Volunteer has been succesfully created!'
        ], 200);
    }

    public function listOfUserVolunteer(Request $request){
        /* Menunjukkan List Event Volunteer dari yang diikuti User Personal */

        $volunteer = UserVolunteer::join('volunteer', 'volunteer.id', '=', 'user_volunteer.volunteer_id')
            ->get();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function registratedUserVolunteer(Request $request, $id){
        /* Menunjukkan List User yang mengikuti Event Volunteer berdasarkan ID eventnya */

        $volunteer = UserVolunteer::join('personal', 'personal.id', '=', 'user_volunteer.personal_id')
            ->where('volunteer_id', '=', $id)
            ->get();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function detailRegistratedUserVolunteer(Request $request, $id){
        /* Menunjukkan detail user yang terdaftar pada suatu Volunteer Event */

        $volunteer = UserVolunteer::join('personal', 'personal.id', '=', 'user_volunteer.personal_id')
            ->where('personal_id', '=', $id)
            ->get();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function acceptUser(Request $request, $id){
        $volunteer = UserVolunteer::where('personal_id', $id)->first();
        $volunteer->status = "Y";

        $volunteer->save();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function declineUser(Request $request, $id){
        $volunteer = UserVolunteer::where('personal_id', $id)->first();
        $volunteer->status = "N";

        $volunteer->save();

        if (!$volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $volunteer
        ], 200);
    }

    public function getFile($name)
    {
        $file_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($file_path);

        if (file_exists($file_path)) {
            $file = File::get($file_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "File not found";

        return $res;
    }

    public function getQrCode($name)
    {
        $file_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($file_path);

        if (file_exists($file_path)) {
            $file = File::get($file_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "File not found";

        return $res;
    }
}