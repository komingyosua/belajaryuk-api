<?php


namespace App\Http\Controllers;

use App\User;
use App\FavoritedEvent;
use Illuminate\Http\Request;

class FavoritedEventController
{
    public function getFavoritedEvent(Request $request){

        $id = User::getPersonalId($request);
        $favorited_event = FavoritedEvent::where('personal_id', $id)->get();

        if (!$favorited_event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $favorited_event
        ], 200);
    }

    public function createFavoritedEvent(Request $request, $event_id){
        $favorited_event = new FavoritedEvent;

        $id = User::getPersonalId($request);
        $favorited_event->personal_id = $id;
        $favorited_event->event_id = $event_id;

        $favorited_event->save();

        if (!$favorited_event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Event has been succesfully created!'
        ], 200);
    }

    public function deleteFavoritedEvent($id)
    {
        $favorited_event = FavoritedEvent::where('id', $id)->first();

        $favorited_event->delete();

        if (!$favorited_event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Event has been succesfully deleted!'
        ], 200);
    }
}