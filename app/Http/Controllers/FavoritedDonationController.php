<?php


namespace App\Http\Controllers;


use App\FavoritedDonation;
use App\User;
use Illuminate\Http\Request;

class FavoritedDonationController
{
    public function getFavoritedDonation(Request $request){

        $id = User::getPersonalId($request);
        $favorited_donation = FavoritedDonation::where('personal_id', $id)->get();

        if (!$favorited_donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $favorited_donation
        ], 200);
    }

    public function createFavoritedDonation(Request $request, $donation_id){
        $favorited_donation = new FavoritedDonation;

        $id = User::getPersonalId($request);
        $favorited_donation->personal_id = $id;
        $favorited_donation->donation_id = $donation_id;

        $favorited_donation->save();

        if (!$favorited_donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Donation has been succesfully created!'
        ], 200);
    }

    public function deleteFavoritedDonation($id)
    {
        $favorited_donation = FavoritedDonation::where('id', $id)->first();

        $favorited_donation->delete();

        if (!$favorited_donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Donation has been succesfully deleted!'
        ], 200);
    }
}