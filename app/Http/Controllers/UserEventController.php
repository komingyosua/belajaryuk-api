<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\UserEvent;

class UserEventController extends Controller
{

    public function __construct()
    {

    }

    public function createUserEvent(Request $request, $id)
    {

        $userEvent = new UserEvent;

        $userEvent->personal_id = User::getPersonalId($request);
        $userEvent->event_id = $id;

        $userEvent->save();

        if (!$userEvent)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'User Event has been succesfully created!'
        ], 200);
    }

    public function listOfUserEvent(Request $request){
        $event = UserEvent::join('event', 'event.id', '=', 'user_event.event_id')->get();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event
        ], 200);
    }

    public function registratedUserEvent(Request $request, $id){
        $event = UserEvent::join('personal', 'personal.id', '=', 'user_event.personal_id')
            ->where('event_id', '=', $id)
            ->get();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event
        ], 200);
    }

    public function detailRegistratedUserEvent(Request $request, $id){
        $event = UserEvent::join('personal', 'personal.id', '=', 'user_event.personal_id')
            ->where('personal_id', '=', $id)
            ->get();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event
        ], 200);
    }

}