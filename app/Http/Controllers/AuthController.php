<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\User;
use App\Personal;
use App\Community;

class AuthController extends BaseController
{

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "backend-api",
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + 525600
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function authenticate(User $user)
    {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        $user = User::where('email', $this->request->input('email'))->first();

        if (!$user)
        {
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }

        if (Hash::check($this->request->input('password'), $user->password))
        {
            return response()->json([
                'token' => $this->jwt($user),
                'role' => $user->role,
                'user' => $user
            ], 200);
        }

        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }

    public function register(Request $request)
    {
        $this->validate($this->request, [
            'name'      => 'required',
            'email'     => 'required|email',
            'password'  => 'required',
            'role'      => 'required'
        ]);

        $user = User::create($request->except('name'));

        if($user->role == "personal")
        {
            $personal = new Personal;
            $personal->name = $request->input('name');
            $personal->user_id = $user->id;

            $personal->save();
        }else if($user->role == "community")
        {
            $community = new Community;
            $community->name = $request->input('name');
            $community->user_id = $user->id;

            $community->save();
        }

        if (!$user)
        {
            return response()->json([
                'error' => 'Problem occured during registration!'
            ], 400);
        }

        return response()->json([
            'success' => 'User has been successfully added!'
        ], 200);
    }

    public function getUser($id) {
        $user = User::where('id', $id)->first();

        if (!$user)
        {
            return response()->json([
                'error' => 'Problem!'
            ], 400);
        }

        return response()->json([
            'success' => $user
        ], 200);
    }

    public function changePassword(Request $request, $id){

        $user = User::where('id', $id)->first();

        $old_password = Hash::make($request->input('old_password'));

        if(Hash::check($user->password, $old_password)) {
            if(strcmp($request->input('new_password'),$request->input('confirm_password')) == 0) {
              $user->password = $request->input('new_password');
            }
        }

        $user->save();

        if (!$user)
        {
            return response()->json([
                'error' => 'Problem!'
            ], 400);
        }

        return response()->json([
            'success' => "Password has been changed!",
            'password' => $user->password
        ], 200);

    }
}