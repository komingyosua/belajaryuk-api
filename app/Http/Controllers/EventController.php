<?php

namespace App\Http\Controllers;

use App\Traits\UsesUpload;
use Illuminate\Http\Request;
use App\Event;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class EventController extends Controller
{

    use UsesUpload;

    public function __construct()
    {

    }

    public function listAllEvent()
    {
        $event = Event::get();
        $eventCount = count($event);

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event,
            'totalEvent' => $eventCount
        ], 200);
    }

    public function highlightEvent() {
        $event = Event::all()->take(3);

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event,
        ], 200);
    }

    public function listEventByUser($id)
    {
        $event = Event::where('user_id', $id)->get();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event
        ], 200);
    }

    public function detailEvent($id)
    {
        $event = Event::where('id', $id)->get();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $event
        ], 200);
    }

    public function createEvent(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'desc'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $event = new Event;
        $event->community_id = User::getCommunityId($request);
        $event->title = $request->input('title');
        $event->desc = $request->input('desc');
        $event->date = $request->input('date');
        $event->time = $request->input('time');
        $event->place = $request->input('place');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $event->picture = $picName;
        }

        $event->save();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Event has been succesfully created!'
        ], 200);
    }

    public function updateEvent(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'desc'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $event = Event::where('id', $id)->first();

        $event->title = $request->input('title');
        $event->desc = $request->input('desc');
        $event->date = $request->input('date');
        $event->time = $request->input('time');
        $event->place = $request->input('place');

        if($request->has('picture'))
        {
            $curr_picture_path = storage_path('app/public') . '/' . $event->picture;
            if (file_exists($curr_picture_path)){
                unlink($curr_picture_path);
            }
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $event->picture = $picName;
        }

        $event->save();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Event has been succesfully updated!'
        ], 200);
    }

    public function deleteEvent($id)
    {
        $event = Event::where('id',$id)->first();

        $event->delete();

        if (!$event)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Event has been succesfully deleted!'
        ], 200);
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }
}