<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ArticleController extends Controller
{

    public function __construct()
    {

    }

    public function listAllArticle()
    {
        $article = Article::get();

        if (!$article)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $article
        ], 200);
    }

    public function detailArticle($id)
    {
        $article = Article::where('id', $id)->get();

        if (!$article)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $article
        ], 200);
    }

    public function createArticle(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'description'      => 'required',
        ]);

        $article = new Article;

        $article->title = $request->input('title');
        $article->description = $request->input('description');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('uploads/volunteers/images');
            $request->file('picture')->move($destinationPath, $picName);
            $article->picture = $picName;
        }

        $article->save();


        if (!$article)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Article has been succesfully created!'
        ], 200);
    }

    public function updateArticle(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'description'      => 'required',
        ]);

        $article = Article::where('id', $id)->first();

        $article->title = $request->input('title');
        $article->description = $request->input('description');

        if($request->has('picture'))
        {
            $curr_picture_path = storage_path('uploads/articles/images') . '/' . $article->picture;
            if (file_exists($curr_picture_path)){
                unlink($curr_picture_path);
            }
            $picName = Str::random(34);
            $destinationPath = storage_path('uploads/articles/images');
            $request->file('picture')->move($destinationPath, $picName);
            $article->picture = $picName;
        }

        $article->save();

        if (!$article)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Article has been succesfully updated!'
        ], 200);
    }

    public function deleteArticle($id)
    {
        $article = Article::where('id',$id)->first();

        $article->delete();

        if (!$article)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Article has been succesfully deleted!'
        ], 200);
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('uploads/articles/images') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }
}