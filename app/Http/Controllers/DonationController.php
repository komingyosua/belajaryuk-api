<?php

namespace App\Http\Controllers;

use App\Donation;
use App\Traits\UsesUpload;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class DonationController extends Controller
{
    use UsesUpload;

    public function __construct()
    {

    }

    public function listAllDonation()
    {
        $donation = Donation::get();
        $donationCount = count($donation);

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation,
            'totalDonation' => $donationCount
        ], 200);
    }

    public function highlightDonation()
    {
        $donation = Donation::all()->take(3);

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function listDonationByUser($id)
    {
        $donation = Donation::where('user_id', $id)->get();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function detailDonation($id)
    {
        $donation = Donation::where('id', $id)->get();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $donation
        ], 200);
    }

    public function createDonation(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'desc'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'pool' => 'required',
            'creator' => 'required',
            'category' => 'required',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $donation = new Donation;
        $donation->community_id = User::getCommunityId($request);
        $donation->title = $request->input('title');
        $donation->desc = $request->input('desc');
        $donation->date = $request->input('date');
        $donation->time = $request->input('time');
        $donation->place = $request->input('place');
        $donation->pool = $request->input('pool');
        $donation->creator = $request->input('creator');
        $donation->category = $request->input('category');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $donation->picture = $picName;
        }

        $donation->save();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Donation has been succesfully created!'
        ], 200);
    }

    public function updateDonation(Request $request, $id)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'description'      => 'required',
            'date'      => 'required',
            'time'      => 'required',
            'place'     => 'required',
            'pool' => 'required',
            'creator' => 'required',
            'category' => 'required',
            'picture'   => 'required|image|mimes:jpeg,jpg,png'
        ]);

        $donation = Donation::where('id', $id)->first();

        $donation->title = $request->input('title');
        $donation->desc = $request->input('desc');
        $donation->date = $request->input('date');
        $donation->time = $request->input('time');
        $donation->place = $request->input('place');
        $donation->pool = $request->input('pool');
        $donation->creator = $request->input('creator');
        $donation->category = $request->input('category');

        if($request->has('picture'))
        {
            $curr_picture_path = storage_path('app/public') . '/' . $donation->picture;
            if (file_exists($curr_picture_path)){
                unlink($curr_picture_path);
            }
            $picName = Str::random(34);
            $destinationPath = storage_path('app/public');
            $request->file('picture')->move($destinationPath, $picName);
            $donation->picture = $picName;
        }

        $donation->save();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Donation has been succesfully updated!'
        ], 200);
    }

    public function deleteDonation($id)
    {
        $donation = Donation::where('id',$id)->first();

        $donation->delete();

        if (!$donation)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Donation has been succesfully deleted!'
        ], 200);
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('app/public') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }
}