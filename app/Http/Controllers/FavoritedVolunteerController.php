<?php


namespace App\Http\Controllers;


use App\FavoritedVolunteer;
use App\User;
use Illuminate\Http\Request;

class FavoritedVolunteerController
{
    public function getFavoritedVolunteer(Request $request){

        $id = User::getPersonalId($request);
        $favorited_volunteer = FavoritedVolunteer::where('personal_id', $id)->get();

        if (!$favorited_volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $favorited_volunteer
        ], 200);
    }

    public function createFavoritedVolunteer(Request $request, $volunteer_id){
        $favorited_volunteer = new FavoritedVolunteer;

        $id = User::getPersonalId($request);
        $favorited_volunteer->personal_id = $id;
        $favorited_volunteer->volunteer_id = $volunteer_id;

        $favorited_volunteer->save();

        if (!$favorited_volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Volunteer has been succesfully created!'
        ], 200);
    }

    public function deleteFavoritedVolunteer($id)
    {
        $favorited_volunteer = FavoritedVolunteer::where('id', $id)->first();

        $favorited_volunteer->delete();

        if (!$favorited_volunteer)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => 'Favorited Volunteer has been succesfully deleted!'
        ], 200);
    }
}