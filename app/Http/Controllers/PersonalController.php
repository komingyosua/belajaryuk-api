<?php

namespace App\Http\Controllers;

use App\Personal;
use App\User;
use Illuminate\Http\Request;

use App\Community;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class PersonalController extends Controller
{

    public function detailPersonal(Request $request)
    {
        $personal = Personal::where('id', User::getPersonalId($request))->get();

        if (!$personal)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $personal
        ], 200);
    }

    public function detailPersonalById(Request $request, $id)
    {
        $personal = Personal::where('id', $id)->first();

        if (!$personal)
        {
            return response()->json([
                'error' => 'Problem occured!'
            ], 400);
        }

        return response()->json([
            'success' => $personal
        ], 200);
    }

    public function updateProfilePersonal(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:255',
            'address' => 'max:255',
            'city' => 'max:64',
            'province' => 'max:64',
            'post_code' => 'numeric',
            'dob' => 'date',
            'gender' => 'max:10',
            'bio' => 'max:255',
            'phone' => 'numeric',
            'picture' => 'image|mimes:jpg,jpeg,png'
        ]);

        $id = User::getPersonalId($request);

        $personal = Personal::where('id', '=', $id)->first();

        $personal->name = $request->input('name');
        $personal->address = $request->input('address');
        $personal->city = $request->input('city');
        $personal->province = $request->input('province');
        $personal->post_code = $request->input('post_code');
        $personal->dob = $request->input('dob');
        $personal->gender = $request->input('gender');
        $personal->bio = $request->input('bio');
        $personal->phone = $request->input('phone');

        if($request->has('picture'))
        {
            $picName = Str::random(34);
            $destinationPath = storage_path('uploads/personal/images');
            $request->file('picture')->move($destinationPath, $picName);
            $personal->picture = $picName;
        }

        $personal->save();

        if($personal)
        {
            return response()->json([
                'success' => "Success"
            ], 200);
        }
        else
        {
            return response()->json([
                'error' => 'Something wrong!',
            ], 400);
        }
    }

    public function getPicture($name)
    {
        $picture_path = storage_path('uploads/personal/images') . '/' . $name;

        $file_type = File::mimeType($picture_path);

        if (file_exists($picture_path)) {
            $file = File::get($picture_path);
            return response($file, 200)->header('Content-Type', $file_type);
        }

        $res['success'] = false;
        $res['message'] = "Picture not found";

        return $res;
    }
}