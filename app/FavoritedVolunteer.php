<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class FavoritedVolunteer extends Model
{

    use UsesUuid;

    protected $table = 'favorited_volunteer';

    protected $guarded = ['id'];

    protected $fillable = [
        'personal_id',
        'volunteer_id'
    ];
}
