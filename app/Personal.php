<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Personal extends Model
{

    use UsesUuid;

    protected $table = 'personal';

    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'address',
        'city',
        'province',
        'post_code',
        'dob',
        'phone',
        'picture'
    ];

}
