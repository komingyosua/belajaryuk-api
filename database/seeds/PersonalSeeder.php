<?php

use Illuminate\Database\Seeder;
use App\Personal;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Personal::create([
            'name' => 'Yosua',
            'email' => 'yosua@gmail.com',
            'password' => app('hash')->make('test1234'),
            'address' => 'Jl. Rawa Belong',
            'city' => 'Jakarta Barat',
            'province' => 'DKI Jakarta',
            'post_code' => '11530',
            'dob' => '02/02/1998',
            'gender' => 'Male',
            'bio' => 'TFI',
            'phone' => '081237675701',
            'picture' => 'asdasd.jpg'
        ]);
    }
}
