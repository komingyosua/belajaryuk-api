<?php

use Illuminate\Database\Seeder;

use App\Community;

class CommunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Community::create([
            'name' => 'TFI - Teach for Indonesia',
            'email' => 'teach@gmail.com',
            'password' => app('hash')->make('test1234'),
            'address' => 'Jl. Rawa Belong',
            'city' => 'Jakarta Barat',
            'province' => 'DKI Jakarta',
            'post_code' => '11530',
            'bio' => 'TFI',
            'phone' => '081237675701',
            'picture' => 'asdasd.jpg'
        ]);
    }
}
