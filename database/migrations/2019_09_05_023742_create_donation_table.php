<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('community_id');
            $table->string('title');
            $table->string('desc');
            $table->date('date');
            $table->time('time');
            $table->string('place');
            $table->bigInteger('pool');
            $table->string('creator');
            $table->string('category');
            $table->string('picture');
            $table->timestamps();

            $table->foreign('community_id')->references('id')->on('community');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donation');
    }
}
