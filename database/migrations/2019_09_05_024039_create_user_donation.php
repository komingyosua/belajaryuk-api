<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDonation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_donation', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('personal_id');
            $table->uuid('donation_id');
            $table->bigInteger('total_given');
            $table->string('payment_proof')->nullable();
            $table->enum('status', ['Y', 'N', 'P']);
            $table->timestamps();

            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('donation_id')->references('id')->on('donation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_donation');
    }
}
