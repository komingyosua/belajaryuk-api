<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FavoritedDonation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorited_donation', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('personal_id');
            $table->uuid('donation_id');
            $table->timestamps();

            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('donation_id')->references('id')->on('donation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
