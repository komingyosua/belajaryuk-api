<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVolunteer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_volunteer', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('personal_id');
            $table->uuid('volunteer_id');
            $table->string('reason');
            $table->string('other_document');
            $table->string('qr_code')->nullable();
            $table->enum('status', ['Y', 'N', 'P']);
            $table->timestamps();

            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('volunteer_id')->references('id')->on('volunteer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_volunteer');
    }
}
