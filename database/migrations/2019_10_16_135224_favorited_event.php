<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FavoritedEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorited_event', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('personal_id');
            $table->uuid('event_id');
            $table->timestamps();

            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('event_id')->references('id')->on('event');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
